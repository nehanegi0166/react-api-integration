import React from "react";

class AddContact extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="ui main container">
                <h2>Add Contact {this.props.sname}</h2>
                <form className="ui form">
                    <div className="field">
                        <label>Name</label>
                        <input type="text" name="name" placeholder="Name" />
                    </div>
                    <div className="field">
                        <label>Email</label>
                        <input type="text" name="email" placeholder="Email" />
                    </div>
                    <button className="ui button blue">Add</button>
                </form>
            </div>
        );
    }
}

export default AddContact;