import React from 'react';
import './App.css';
import Header from './Header';
import AddContact from './AddContact';
import ContactList from './ContactList';
import Information from './Information';

function App() {

  const contacts =[
    {
      id: "1",
      "name": "Neha",
      "email": "neha@gmail.com",
    },
    {
      id: "2",
      "name": "Monal",
      "email": "monal@gmail.com",
    }
  ];
  const name ="Neha";
  
  return (
    <div className='ui container'>
        <Header />
        <AddContact />
        <ContactList contacts={contacts}/>
        <Information info = {name}/>
    </div>
  );
}

export default App;
